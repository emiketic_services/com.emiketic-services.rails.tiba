Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # Begin routes for all forms in Tiba
  resources :promotions, only: [:show] do
    resources :promo_quotes, only: [:create]
  end
  resources :newsletters, only: [:new, :create]
  resources :contacts, only: [:new, :create]
  resources :quotes, only: [:new, :create] do
    get 'find_appartements', on: :collection
  end
  # End


  resources :references, controller: "past_projects", only: [:index]

	resources :projects, only: [:index, :show] do
    get 'promotions', on: :collection
    get 'villas', on: :collection
    get 'apartments', on: :collection
    get 'commerces', on: :collection
    get 'lotissements', on: :collection
    get 'get_urls'
    get 'plan', on: :member
    resources :products, only: [:show]
    resources :credit_simulators, only: [:index]
  end

  get 'contact', to: 'static_pages#contact', as: 'contact'
  get 'service', to: 'static_pages#service', as: 'service'
  get 'about',   to: 'static_pages#about',   as: 'about'

  root 'static_pages#home'
end
