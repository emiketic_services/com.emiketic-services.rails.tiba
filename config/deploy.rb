# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'tiba'
set :repo_url, 'git@bitbucket.org:emiketic_services/com.emiketic-services.rails.tiba.git'
set :branch, 'master'
set :deploy_to, '/home/deploy/tiba'

set :linked_files, fetch(:linked_files, []).push('config/application.yml', 'config/database.yml', 'config/secrets.yml')
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    invoke 'unicorn:reload'
    # on roles(:app), in: :sequence, wait: 5 do
    #   execute :touch, release_path.join('tmp/restart.txt')
    # end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end
