# == Schema Information
#
# Table name: promotions
#
#  id                         :integer          not null, primary key
#  title                      :string
#  desc                       :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  landing_image_file_name    :string
#  landing_image_content_type :string
#  landing_image_file_size    :integer
#  landing_image_updated_at   :datetime
#  theater_image_file_name    :string
#  theater_image_content_type :string
#  theater_image_file_size    :integer
#  theater_image_updated_at   :datetime
#  featured                   :boolean
#  available                  :boolean          default(TRUE)
#  active_landing             :boolean          default(TRUE)
#  active_theater             :boolean          default(TRUE)
#  promo_link                 :string
#

require 'test_helper'

class PromotionsControllerTest < ActionController::TestCase
  test "should get show" do
    get :show
    assert_response :success
  end

end
