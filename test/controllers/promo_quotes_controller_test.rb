# == Schema Information
#
# Table name: promo_quotes
#
#  id           :integer          not null, primary key
#  promotion_id :integer
#  first_name   :string
#  last_name    :string
#  email        :string
#  phone        :string
#  code_postal  :string
#  loc          :string
#  pays         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  ville        :string
#

require 'test_helper'

class PromoQuotesControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

end
