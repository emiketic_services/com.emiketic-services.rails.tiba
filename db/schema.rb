# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161216161146) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "albums", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "country"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "message"
    t.string   "ville"
  end

  create_table "credit_simulators", force: :cascade do |t|
    t.integer  "personal_apport"
    t.string   "remboursement_period"
    t.integer  "remboursement_duration"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "filter_configurations", force: :cascade do |t|
    t.integer  "max_surf"
    t.integer  "min_surf"
    t.integer  "min_price"
    t.integer  "max_price"
    t.integer  "min_room"
    t.integer  "max_room"
    t.integer  "nb_floor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "project_id"
  end

  add_index "filter_configurations", ["project_id"], name: "index_filter_configurations_on_project_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "gallery_photos", force: :cascade do |t|
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "project_id"
  end

  add_index "gallery_photos", ["project_id"], name: "index_gallery_photos_on_project_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.integer  "album_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "content_file_name"
    t.string   "content_content_type"
    t.integer  "content_file_size"
    t.datetime "content_updated_at"
  end

  add_index "images", ["album_id"], name: "index_images_on_album_id", using: :btree

  create_table "meta_configurations", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "project_id"
  end

  add_index "meta_configurations", ["project_id"], name: "index_meta_configurations_on_project_id", using: :btree

  create_table "newsletters", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "past_projects", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "featured_image_file_name"
    t.string   "featured_image_content_type"
    t.integer  "featured_image_file_size"
    t.datetime "featured_image_updated_at"
  end

  create_table "phone_numbers", force: :cascade do |t|
    t.string   "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "phone_id"
  end

  add_index "phone_numbers", ["phone_id"], name: "index_phone_numbers_on_phone_id", using: :btree

  create_table "phones", force: :cascade do |t|
    t.string   "number"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "vcard_file_name"
    t.string   "vcard_content_type"
    t.integer  "vcard_file_size"
    t.datetime "vcard_updated_at"
  end

  create_table "photos", force: :cascade do |t|
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "overview"
    t.integer  "superficie"
    t.integer  "configuration"
    t.integer  "floor"
    t.integer  "project_id"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "sold",                         default: false, null: false
    t.string   "slug"
    t.decimal  "price"
    t.string   "plan_image_file_name"
    t.string   "plan_image_content_type"
    t.integer  "plan_image_file_size"
    t.datetime "plan_image_updated_at"
    t.text     "description_technique"
    t.string   "document_pdf_file_name"
    t.string   "document_pdf_content_type"
    t.integer  "document_pdf_file_size"
    t.datetime "document_pdf_updated_at"
    t.integer  "rank"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "related_picture_file_name"
    t.string   "related_picture_content_type"
    t.integer  "related_picture_file_size"
    t.datetime "related_picture_updated_at"
    t.string   "product_plan_file_name"
    t.string   "product_plan_content_type"
    t.integer  "product_plan_file_size"
    t.datetime "product_plan_updated_at"
  end

  add_index "products", ["project_id"], name: "index_products_on_project_id", using: :btree
  add_index "products", ["slug"], name: "index_products_on_slug", unique: true, using: :btree

  create_table "project_videos", force: :cascade do |t|
    t.string   "video_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "project_id"
  end

  add_index "project_videos", ["project_id"], name: "index_project_videos_on_project_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "title"
    t.text     "overview"
    t.text     "description"
    t.string   "location"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "plan_image_file_name"
    t.string   "plan_image_content_type"
    t.integer  "plan_image_file_size"
    t.datetime "plan_image_updated_at"
    t.string   "category"
    t.string   "slug"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "promotion_id"
    t.string   "featured_image_file_name"
    t.string   "featured_image_content_type"
    t.integer  "featured_image_file_size"
    t.datetime "featured_image_updated_at"
    t.string   "video_url"
    t.string   "map_url"
    t.integer  "rank"
    t.boolean  "available",                   default: true
    t.string   "plan_pdf_file_name"
    t.string   "plan_pdf_content_type"
    t.integer  "plan_pdf_file_size"
    t.datetime "plan_pdf_updated_at"
    t.string   "brochure_pdf_file_name"
    t.string   "brochure_pdf_content_type"
    t.integer  "brochure_pdf_file_size"
    t.datetime "brochure_pdf_updated_at"
    t.integer  "max_surf"
    t.integer  "min_surf"
    t.integer  "min_price"
    t.integer  "max_price"
    t.integer  "min_room"
    t.integer  "max_room"
    t.integer  "nb_floor"
    t.boolean  "sold",                        default: false
    t.boolean  "show_area",                   default: true
    t.boolean  "show_price",                  default: true
    t.boolean  "show_room",                   default: true
    t.boolean  "show_floor",                  default: true
    t.integer  "price"
    t.string   "vcard_file_name"
    t.string   "vcard_content_type"
    t.integer  "vcard_file_size"
    t.datetime "vcard_updated_at"
  end

  add_index "projects", ["promotion_id"], name: "index_projects_on_promotion_id", using: :btree
  add_index "projects", ["slug"], name: "index_projects_on_slug", unique: true, using: :btree

  create_table "projects_headers", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "promo_quotes", force: :cascade do |t|
    t.integer  "promotion_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "code_postal"
    t.string   "loc"
    t.string   "pays"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "ville"
  end

  add_index "promo_quotes", ["promotion_id"], name: "index_promo_quotes_on_promotion_id", using: :btree

  create_table "promotions", force: :cascade do |t|
    t.string   "title"
    t.text     "desc"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "landing_image_file_name"
    t.string   "landing_image_content_type"
    t.integer  "landing_image_file_size"
    t.datetime "landing_image_updated_at"
    t.string   "theater_image_file_name"
    t.string   "theater_image_content_type"
    t.integer  "theater_image_file_size"
    t.datetime "theater_image_updated_at"
    t.boolean  "featured"
    t.boolean  "available",                  default: true
    t.boolean  "active_landing",             default: true
    t.boolean  "active_theater",             default: true
    t.string   "promo_link"
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "project_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "avenue"
    t.string   "code_postal"
    t.string   "loc"
    t.string   "pays"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "product_id"
  end

  add_index "quotes", ["product_id"], name: "index_quotes_on_product_id", using: :btree
  add_index "quotes", ["project_id"], name: "index_quotes_on_project_id", using: :btree

  create_table "slides", force: :cascade do |t|
    t.integer  "slideshow_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "project_id"
  end

  add_index "slides", ["project_id"], name: "index_slides_on_project_id", using: :btree
  add_index "slides", ["slideshow_id"], name: "index_slides_on_slideshow_id", using: :btree

  create_table "slideshows", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "social_networks", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "activated",  default: true
  end

  create_table "team_members", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.string   "email"
    t.text     "bio"
    t.string   "linkedin"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "rank"
  end

  create_table "testimonials", force: :cascade do |t|
    t.string   "category"
    t.text     "description"
    t.string   "name"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "hide",                default: true
  end

  create_table "videohomes", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "videos", force: :cascade do |t|
    t.string   "url"
    t.integer  "videohome_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "videos", ["videohome_id"], name: "index_videos_on_videohome_id", using: :btree

  add_foreign_key "filter_configurations", "projects"
  add_foreign_key "gallery_photos", "projects"
  add_foreign_key "images", "albums"
  add_foreign_key "meta_configurations", "projects"
  add_foreign_key "phone_numbers", "phones"
  add_foreign_key "products", "projects"
  add_foreign_key "project_videos", "projects"
  add_foreign_key "projects", "promotions"
  add_foreign_key "promo_quotes", "promotions"
  add_foreign_key "quotes", "products"
  add_foreign_key "quotes", "projects"
  add_foreign_key "slides", "projects"
  add_foreign_key "slides", "slideshows"
  add_foreign_key "videos", "videohomes"
end
