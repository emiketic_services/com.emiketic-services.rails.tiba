class CreatePromoQuotes < ActiveRecord::Migration
  def change
    create_table :promo_quotes do |t|
      t.references :promotion, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :code_postal
      t.string :loc
      t.string :pays

      t.timestamps null: false
    end
  end
end
