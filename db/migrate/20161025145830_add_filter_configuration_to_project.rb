class AddFilterConfigurationToProject < ActiveRecord::Migration
  def change
    add_column :projects, :max_surf, :integer
    add_column :projects, :min_surf, :integer
    add_column :projects, :min_price, :integer
    add_column :projects, :max_price, :integer
    add_column :projects, :min_room, :integer
    add_column :projects, :max_room, :integer
    add_column :projects, :nb_floor, :integer
  end
end
