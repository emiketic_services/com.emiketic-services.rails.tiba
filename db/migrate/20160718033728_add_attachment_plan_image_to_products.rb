class AddAttachmentPlanImageToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :plan_image
    end
  end

  def self.down
    remove_attachment :products, :plan_image
  end
end
