class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.references :slideshow, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
