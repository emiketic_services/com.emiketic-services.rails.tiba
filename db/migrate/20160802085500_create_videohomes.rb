class CreateVideohomes < ActiveRecord::Migration
  def change
    create_table :videohomes do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
