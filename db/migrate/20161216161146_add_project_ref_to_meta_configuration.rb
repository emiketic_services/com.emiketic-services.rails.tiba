class AddProjectRefToMetaConfiguration < ActiveRecord::Migration
  def change
    add_reference :meta_configurations, :project, index: true, foreign_key: true
  end
end
