class CreateAppartements < ActiveRecord::Migration
  def change
    create_table :appartements do |t|
      t.string :name
      t.text :overview
      t.integer :superficie
      t.string :configuration
      t.integer :floor
      t.references :project, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
