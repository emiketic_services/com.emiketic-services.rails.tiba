class AddPhoneRefToPhoneNumber < ActiveRecord::Migration
  def change
    add_reference :phone_numbers, :phone, index: true, foreign_key: true
  end
end
