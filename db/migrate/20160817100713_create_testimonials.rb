class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.string :category
      t.text :description
      t.string :name
      t.attachment :image

      t.timestamps null: false
    end
  end
end
