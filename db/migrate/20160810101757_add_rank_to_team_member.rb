class AddRankToTeamMember < ActiveRecord::Migration
  def change
    add_column :team_members, :rank, :integer
  end
end
