class AddMapUrlToProject < ActiveRecord::Migration
  def change
    add_column :projects, :map_url, :string
  end
end
