class CreateProjectVideos < ActiveRecord::Migration
  def change
    create_table :project_videos do |t|
      t.string :video_url

      t.timestamps null: false
    end
  end
end
