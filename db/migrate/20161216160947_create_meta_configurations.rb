class CreateMetaConfigurations < ActiveRecord::Migration
  def change
    create_table :meta_configurations do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
