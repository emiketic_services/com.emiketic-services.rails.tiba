class AddPromotionToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :promoted, :boolean, null: false, default: false
  end
end
