class AddPromoLinkToPromotion < ActiveRecord::Migration
  def change
    add_column :promotions, :promo_link, :string
  end
end
