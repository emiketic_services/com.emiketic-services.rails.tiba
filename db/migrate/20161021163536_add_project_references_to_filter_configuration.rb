class AddProjectReferencesToFilterConfiguration < ActiveRecord::Migration
  def change
    add_reference :filter_configurations, :project, index: true, foreign_key: true
  end
end
