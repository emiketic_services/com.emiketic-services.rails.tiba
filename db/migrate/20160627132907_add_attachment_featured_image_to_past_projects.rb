class AddAttachmentFeaturedImageToPastProjects < ActiveRecord::Migration
  def self.up
    change_table :past_projects do |t|
      t.attachment :featured_image
    end
  end

  def self.down
    remove_attachment :past_projects, :featured_image
  end
end
