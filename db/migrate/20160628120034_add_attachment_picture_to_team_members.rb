class AddAttachmentPictureToTeamMembers < ActiveRecord::Migration
  def self.up
    change_table :team_members do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :team_members, :picture
  end
end
