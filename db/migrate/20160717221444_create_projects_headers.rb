class CreateProjectsHeaders < ActiveRecord::Migration
  def change
    create_table :projects_headers do |t|
      t.text :content

      t.timestamps null: false
    end
  end
end
