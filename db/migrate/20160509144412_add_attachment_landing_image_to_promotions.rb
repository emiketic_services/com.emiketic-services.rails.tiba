class AddAttachmentLandingImageToPromotions < ActiveRecord::Migration
  def self.up
    change_table :promotions do |t|
      t.attachment :landing_image
    end
  end

  def self.down
    remove_attachment :promotions, :landing_image
  end
end
