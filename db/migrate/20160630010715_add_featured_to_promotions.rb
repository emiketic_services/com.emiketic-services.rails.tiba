class AddFeaturedToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :featured, :boolean
  end
end
