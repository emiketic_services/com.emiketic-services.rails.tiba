class AddSoldedToAppartements < ActiveRecord::Migration
  def change
    add_column :appartements, :solded, :boolean, null: false, default: false
  end
end
