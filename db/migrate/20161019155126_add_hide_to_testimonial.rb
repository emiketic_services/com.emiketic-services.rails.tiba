class AddHideToTestimonial < ActiveRecord::Migration
  def change
    add_column :testimonials, :hide, :boolean, null: true, default: true
  end
end
