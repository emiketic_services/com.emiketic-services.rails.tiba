class CreateFilterConfigurations < ActiveRecord::Migration
  def change
    create_table :filter_configurations do |t|
      t.integer :max_surf
      t.integer :min_surf
      t.integer :min_price
      t.integer :max_price
      t.integer :min_room
      t.integer :max_room
      t.integer :nb_floor

      t.timestamps null: false
    end
  end
end
