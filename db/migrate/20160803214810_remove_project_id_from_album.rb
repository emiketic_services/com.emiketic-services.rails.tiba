class RemoveProjectIdFromAlbum < ActiveRecord::Migration
  def change
    remove_column :albums, :project_id, :string
  end
end
