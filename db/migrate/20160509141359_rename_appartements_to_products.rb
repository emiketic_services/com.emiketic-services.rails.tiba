class RenameAppartementsToProducts < ActiveRecord::Migration
  def change
    rename_table :appartements, :products
  end
end
