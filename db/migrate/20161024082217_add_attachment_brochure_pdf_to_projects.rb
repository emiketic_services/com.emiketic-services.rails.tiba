class AddAttachmentBrochurePdfToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :brochure_pdf
    end
  end

  def self.down
    remove_attachment :projects, :brochure_pdf
  end
end
