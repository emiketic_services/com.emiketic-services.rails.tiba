class AddSoldToProject < ActiveRecord::Migration
  def change
    add_column :projects, :sold, :boolean, null: true, default: false
  end
end
