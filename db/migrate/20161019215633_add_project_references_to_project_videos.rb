class AddProjectReferencesToProjectVideos < ActiveRecord::Migration
  def change
    add_reference :project_videos, :project, index: true, foreign_key: true
  end
end
