class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.string :name
      t.string :title
      t.string :email
      t.text :bio
      t.string :linkedin

      t.timestamps null: false
    end
  end
end
