class AddAttachmentImageToProjectsHeaders < ActiveRecord::Migration
  def self.up
    change_table :projects_headers do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :projects_headers, :image
  end
end
