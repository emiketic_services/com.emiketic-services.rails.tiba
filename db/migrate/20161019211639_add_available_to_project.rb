class AddAvailableToProject < ActiveRecord::Migration
  def change
    add_column :projects, :available, :boolean, null: true, default: true
  end
end
