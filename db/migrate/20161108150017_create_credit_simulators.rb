class CreateCreditSimulators < ActiveRecord::Migration
  def change
    create_table :credit_simulators do |t|
      t.integer :personal_apport
      t.string :remboursement_period
      t.integer :remboursement_duration

      t.timestamps null: false
    end
  end
end
