class AddDescriptionTechniqueToProducts < ActiveRecord::Migration
  def change
    add_column :products, :description_technique, :text
  end
end
