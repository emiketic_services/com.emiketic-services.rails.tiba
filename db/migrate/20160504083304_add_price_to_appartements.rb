class AddPriceToAppartements < ActiveRecord::Migration
  def change
    add_column :appartements, :price, :decimal
  end
end
