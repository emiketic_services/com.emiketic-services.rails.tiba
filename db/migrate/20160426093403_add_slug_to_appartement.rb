class AddSlugToAppartement < ActiveRecord::Migration
  def change
    add_column :appartements, :slug, :string
    add_index :appartements, :slug, unique: true
  end
end
