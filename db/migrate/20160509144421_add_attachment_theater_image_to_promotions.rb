class AddAttachmentTheaterImageToPromotions < ActiveRecord::Migration
  def self.up
    change_table :promotions do |t|
      t.attachment :theater_image
    end
  end

  def self.down
    remove_attachment :promotions, :theater_image
  end
end
