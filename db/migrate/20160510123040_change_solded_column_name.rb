class ChangeSoldedColumnName < ActiveRecord::Migration
  def change
    rename_column :products, :solded, :sold
  end
end
