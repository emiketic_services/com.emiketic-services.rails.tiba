class AddActiveToPromotion < ActiveRecord::Migration
  def change
    add_column :promotions, :active_landing, :boolean, null: true, default: true
    add_column :promotions, :active_theater, :boolean, null: true, default: true
  end
end
