class AddVisibleFilterToProject < ActiveRecord::Migration
  def change
    add_column :projects, :show_area, :boolean, null: true, default: true
    add_column :projects, :show_price, :boolean, null: true, default: true
    add_column :projects, :show_room, :boolean, null: true, default: true
    add_column :projects, :show_floor, :boolean, null: true, default: true
  end
end
