class AddAttachmentPlanPdfToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :plan_pdf
    end
  end

  def self.down
    remove_attachment :projects, :plan_pdf
  end
end
