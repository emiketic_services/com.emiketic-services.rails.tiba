class AddProductReferencesToQuotes < ActiveRecord::Migration
  def change
    add_reference :quotes, :product, index: true, foreign_key: true
  end
end
