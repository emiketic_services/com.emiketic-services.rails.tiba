class AddAttachmentRelatedPictureToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :related_picture
    end
  end

  def self.down
    remove_attachment :products, :related_picture
  end
end
