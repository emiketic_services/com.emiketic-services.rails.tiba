class AddAttachmentVcardToPhones < ActiveRecord::Migration
  def self.up
    change_table :phones do |t|
      t.attachment :vcard
    end
  end

  def self.down
    remove_attachment :phones, :vcard
  end
end
