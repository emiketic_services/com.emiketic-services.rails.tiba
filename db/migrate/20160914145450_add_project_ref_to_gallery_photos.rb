class AddProjectRefToGalleryPhotos < ActiveRecord::Migration
  def change
    add_reference :gallery_photos, :project, index: true, foreign_key: true
  end
end
