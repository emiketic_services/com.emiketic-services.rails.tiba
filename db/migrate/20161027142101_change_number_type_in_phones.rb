class ChangeNumberTypeInPhones < ActiveRecord::Migration
  def change
    change_column :phones, :number, :string
  end
end
