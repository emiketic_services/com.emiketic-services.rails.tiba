class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :title
      t.text :desc

      t.timestamps null: false
    end
  end
end
