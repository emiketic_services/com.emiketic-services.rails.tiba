class CreatePastProjects < ActiveRecord::Migration
  def change
    create_table :past_projects do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
