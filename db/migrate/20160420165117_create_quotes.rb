class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.references :project, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :avenue
      t.string :code_postal
      t.string :loc
      t.string :pays
      t.timestamps null: false
    end
  end
end
