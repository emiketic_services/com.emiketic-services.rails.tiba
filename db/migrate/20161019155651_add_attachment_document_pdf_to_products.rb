class AddAttachmentDocumentPdfToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :document_pdf
    end
  end

  def self.down
    remove_attachment :products, :document_pdf
  end
end
