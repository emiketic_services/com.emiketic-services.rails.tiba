class RemovePromotedFromProject < ActiveRecord::Migration
  def change
    remove_column :projects, :promoted
  end
end
