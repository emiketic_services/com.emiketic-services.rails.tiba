class AddPromotionReferencesToProjects < ActiveRecord::Migration
  def change
    add_reference :projects, :promotion, index: true, foreign_key: true
  end
end
