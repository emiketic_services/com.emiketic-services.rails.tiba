class AddAvailableToPromotion < ActiveRecord::Migration
  def change
    add_column :promotions, :available, :boolean, null: true, default: true
  end
end
