class ModifyAppartementsConfigurationAttributes < ActiveRecord::Migration
  def change
    change_column :appartements, :configuration, 'integer USING CAST(configuration AS integer)'
  end
end
