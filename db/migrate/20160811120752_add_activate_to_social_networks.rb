class AddActivateToSocialNetworks < ActiveRecord::Migration
  def change
    add_column :social_networks, :activated, :boolean, null: true, default: true
  end
end
