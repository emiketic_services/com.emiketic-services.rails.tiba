class AddAttachmentPlanImageToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :plan_image
    end
  end

  def self.down
    remove_attachment :projects, :plan_image
  end
end
