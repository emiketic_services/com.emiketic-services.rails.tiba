# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') unless AdminUser.find_by_email("admin@example.com")

# Social Networks
social_network_names = ['facebook', 'twitter', 'linkedin', 'google-plus', 'instagram', 'youtube']
existing_social_network_names = SocialNetwork.pluck(:name)

social_network_names.each do |name|
  unless existing_social_network_names.include?(name)
    SocialNetwork.create!(name: name)
  end
end

Slideshow.create! unless Slideshow.exists?
ProjectsHeader.create! unless ProjectsHeader.exists?
Album.create!(
  name: "My Home Album",
  images_attributes: [
    {
      content: File.new("#{Rails.root}/app/assets/images/reference1.jpg")
    },
    {
      content: File.new("#{Rails.root}/app/assets/images/reference1.jpg")
    },
    {
      content: File.new("#{Rails.root}/app/assets/images/reference1.jpg")
    }
  ].sample(3)
)

Videohome.create!(
  title: "Change me"
)

if Rails.env.development?
  5.times do
    project = Project.create!(
    title:       Faker::Lorem.words.join(' '),
    overview:    Faker::Lorem.paragraph,
    description: Faker::Lorem.paragraph,
    location: [
      Faker::Address.street_name,
      Faker::Address.street_address,
      Faker::Address.country
    ].join(', '),
    plan_image: File.new("#{Rails.root}/app/assets/images/plan.png"),
    category: Project:: PROJECT_CATEGORIES.sample,
    featured_image:  File.new("#{Rails.root}/app/assets/images/plan.png")
    )
    4.times do
      Product.create!(
      name: Faker::Lorem.words.join(' '),
      overview: Faker::Lorem.paragraph,
      superficie: (100..600).to_a.sample,
      configuration: (1..10).to_a.sample,
      floor: (1..10).to_a.sample,
      price: (10000..500000).to_a.sample,
      plan_image: File.new("#{Rails.root}/app/assets/images/plan.png"),
      description_technique: Faker::Lorem.paragraph,
      project_id: project.id
      )
    end

  end
end
