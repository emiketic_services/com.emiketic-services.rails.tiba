# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  email      :string
#  country    :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  message    :text
#  ville      :string
#

class Contact < ActiveRecord::Base
  validates :first_name, :last_name, :email, :message, presence: true

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  def fullname
    [self.first_name, self.last_name].join(' ')
  end
end
