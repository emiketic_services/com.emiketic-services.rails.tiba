# == Schema Information
#
# Table name: phones
#
#  id                 :integer          not null, primary key
#  number             :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  vcard_file_name    :string
#  vcard_content_type :string
#  vcard_file_size    :integer
#  vcard_updated_at   :datetime
#

class Phone < ActiveRecord::Base

  has_many :phone_numbers
  accepts_nested_attributes_for  :phone_numbers, allow_destroy: true

  has_attached_file :vcard
  validates_attachment_file_name :vcard, matches: [/\.(vcf)\z/]

end
