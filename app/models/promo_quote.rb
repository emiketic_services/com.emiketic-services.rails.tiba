# == Schema Information
#
# Table name: promo_quotes
#
#  id           :integer          not null, primary key
#  promotion_id :integer
#  first_name   :string
#  last_name    :string
#  email        :string
#  phone        :string
#  code_postal  :string
#  loc          :string
#  pays         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  ville        :string
#

class PromoQuote < ActiveRecord::Base
  belongs_to :promotion

  validates :promotion, :first_name, :last_name,
            :email, :phone, presence: true
end
