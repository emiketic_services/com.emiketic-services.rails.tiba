# == Schema Information
#
# Table name: newsletters
#
#  id         :integer          not null, primary key
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Newsletter < ActiveRecord::Base
  validates :email, presence: {message: "Vous devriez introduire l'email"}
  validates :email, uniqueness: {message: "Vous etes deja inscrit aux newsletters"}

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                      message: "Veuillez introduire une adresse email"
end
