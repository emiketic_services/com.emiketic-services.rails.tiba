# == Schema Information
#
# Table name: project_videos
#
#  id         :integer          not null, primary key
#  video_url  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  project_id :integer
#

class ProjectVideo < ActiveRecord::Base

  belongs_to :project
  validates :video_url, presence: true
  
end
