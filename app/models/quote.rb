# == Schema Information
#
# Table name: quotes
#
#  id          :integer          not null, primary key
#  project_id  :integer
#  first_name  :string
#  last_name   :string
#  email       :string
#  phone       :string
#  avenue      :string
#  code_postal :string
#  loc         :string
#  pays        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  product_id  :integer
#

class Quote < ActiveRecord::Base
  belongs_to :project
  belongs_to :product

  validates :project, :first_name, :last_name,
            :email, :phone, presence: true

  def fullname
    [self.first_name, self.last_name].join(' ')
  end

end
