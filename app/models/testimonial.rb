# == Schema Information
#
# Table name: testimonials
#
#  id                  :integer          not null, primary key
#  category            :string
#  description         :text
#  name                :string
#  image_file_name     :string
#  image_content_type  :string
#  image_file_size     :integer
#  image_updated_at    :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  hide                :boolean          default(TRUE)
#

class Testimonial < ActiveRecord::Base

  PROJECT_CATEGORIES = ["villas","appartements","commerces","lotissements/terrains"]

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  scope :villas, -> { where(category: "villas") }
  scope :apartments, -> { where(category: "appartements") }
  scope :commerces, -> { where(category: "commerces") }
  scope :lotissements, -> { where(category: "lotissements/terrains") }

  validates :name, :description, :image, presence: true

  validates :category, uniqueness: { scope: :category,
    message: "cannot have another testimonial for the same category" }, presence: true
end
