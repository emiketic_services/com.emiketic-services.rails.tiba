# == Schema Information
#
# Table name: slideshows
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Slideshow < ActiveRecord::Base
  has_many :slides
  has_many :projects

  accepts_nested_attributes_for :slides, allow_destroy: true

  
end
