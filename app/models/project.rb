# == Schema Information
#
# Table name: projects
#
#  id                          :integer          not null, primary key
#  title                       :string
#  overview                    :text
#  description                 :text
#  location                    :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  plan_image_file_name        :string
#  plan_image_content_type     :string
#  plan_image_file_size        :integer
#  plan_image_updated_at       :datetime
#  category                    :string
#  slug                        :string
#  latitude                    :float
#  longitude                   :float
#  promotion_id                :integer
#  featured_image_file_name    :string
#  featured_image_content_type :string
#  featured_image_file_size    :integer
#  featured_image_updated_at   :datetime
#  video_url                   :string
#  map_url                     :string
#  rank                        :integer
#  available                   :boolean          default(TRUE)
#  plan_pdf_file_name          :string
#  plan_pdf_content_type       :string
#  plan_pdf_file_size          :integer
#  plan_pdf_updated_at         :datetime
#  brochure_pdf_file_name      :string
#  brochure_pdf_content_type   :string
#  brochure_pdf_file_size      :integer
#  brochure_pdf_updated_at     :datetime
#  max_surf                    :integer
#  min_surf                    :integer
#  min_price                   :integer
#  max_price                   :integer
#  min_room                    :integer
#  max_room                    :integer
#  nb_floor                    :integer
#  sold                        :boolean          default(FALSE)
#  show_area                   :boolean          default(TRUE)
#  show_price                  :boolean          default(TRUE)
#  show_room                   :boolean          default(TRUE)
#  show_floor                  :boolean          default(TRUE)
#  price                       :integer
#  vcard_file_name             :string
#  vcard_content_type          :string
#  vcard_file_size             :integer
#  vcard_updated_at            :datetime
#

class Project < ActiveRecord::Base
  extend FriendlyId

  PROJECT_CATEGORIES = ["villas","appartements","commerces","lotissements/terrains"]

  friendly_id :title, use: :slugged
  geocoded_by :location
  after_validation :geocode

  belongs_to :promotion
  has_one :album

  has_many :meta_configurations
  has_many :gallery_photos
  has_many :project_videos
  has_many :products

  scope :promotions, -> { where(promoted: true) }
  scope :villas, -> { where(category: "villas") }
  scope :apartments, -> { where(category: "appartements") }
  scope :commerces, -> { where(category: "commerces") }
  scope :lotissements, -> { where(category: "lotissements/terrains") }

  scope :project_order, -> { order(:rank) }
  scope :available, -> { where(available: true)}

  accepts_nested_attributes_for :project_videos, :allow_destroy => true

  has_attached_file :plan_image, styles: { medium: "300x300>", thumb: "100x100>", large: "1280x1000>" }, default_url: "/images/:style/missing.png"
  has_attached_file :featured_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"

  validates_attachment_content_type :plan_image, content_type: /\Aimage\/.*\Z/
  validates_attachment_content_type :featured_image, content_type: /\Aimage\/.*\Z/

  has_attached_file :plan_pdf
  validates_attachment :plan_pdf, :content_type => { :content_type => %w(application/pdf), message: "Vous devez inserer un document de format PDF" }

  has_attached_file :brochure_pdf
  validates_attachment :brochure_pdf, :content_type => { :content_type => %w(application/pdf), message: "Vous devez inserer un document de format PDF" }

  validates_attachment_presence :plan_image
  validates_attachment_presence :featured_image

  accepts_nested_attributes_for :gallery_photos, allow_destroy: true

  validates :title, :description, :location, :category, presence: true
  scope :not_sold, -> { eager_load(:products).where('products.sold = ? OR projects.sold = ? ', false, false).uniq }
end
