# == Schema Information
#
# Table name: slides
#
#  id                 :integer          not null, primary key
#  slideshow_id       :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  project_id         :integer
#

class Slide < ActiveRecord::Base
  belongs_to :slideshow
  belongs_to :project

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  validates :project, presence: { message: "Should be associated to a project"}
  validates :image, presence: { message: "Should have an image for background"}
end
