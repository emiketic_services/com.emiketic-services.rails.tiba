# == Schema Information
#
# Table name: phone_numbers
#
#  id         :integer          not null, primary key
#  number     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  phone_id   :integer
#

class PhoneNumber < ActiveRecord::Base
  validates :number, uniqueness: true

end
