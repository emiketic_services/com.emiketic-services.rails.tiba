# == Schema Information
#
# Table name: products
#
#  id                           :integer          not null, primary key
#  name                         :string
#  overview                     :text
#  superficie                   :integer
#  configuration                :integer
#  floor                        :integer
#  project_id                   :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  sold                         :boolean          default(FALSE), not null
#  slug                         :string
#  price                        :decimal(, )
#  plan_image_file_name         :string
#  plan_image_content_type      :string
#  plan_image_file_size         :integer
#  plan_image_updated_at        :datetime
#  description_technique        :text
#  document_pdf_file_name       :string
#  document_pdf_content_type    :string
#  document_pdf_file_size       :integer
#  document_pdf_updated_at      :datetime
#  rank                         :integer
#  picture_file_name            :string
#  picture_content_type         :string
#  picture_file_size            :integer
#  picture_updated_at           :datetime
#  related_picture_file_name    :string
#  related_picture_content_type :string
#  related_picture_file_size    :integer
#  related_picture_updated_at   :datetime
#  product_plan_file_name       :string
#  product_plan_content_type    :string
#  product_plan_file_size       :integer
#  product_plan_updated_at      :datetime
#

class Product < ActiveRecord::Base
  belongs_to :project

  extend FriendlyId
  friendly_id :name, use: :slugged

  validates :name, :overview, :superficie, :configuration,
            :floor, :project, :description_technique, presence: true

  has_attached_file :document_pdf
  validates_attachment :document_pdf, :content_type => { :content_type => %w(application/pdf), message: "Vous devez inserer un document de format PDF" }

  has_attached_file :related_picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :related_picture, content_type: /\Aimage\/.*\Z/

  APPARTEMENT_CONFIG = 1..10

  has_attached_file :plan_image, styles: { medium: "300x300>", thumb: "100x100>", large: "1280x1000>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :plan_image, content_type: /\Aimage\/.*\Z/
  validates_attachment_presence :plan_image

  scope :product_order, -> { order(:rank) }
end
