# == Schema Information
#
# Table name: past_projects
#
#  id                          :integer          not null, primary key
#  name                        :string
#  description                 :text
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  featured_image_file_name    :string
#  featured_image_content_type :string
#  featured_image_file_size    :integer
#  featured_image_updated_at   :datetime
#

class PastProject < ActiveRecord::Base
  has_attached_file :featured_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"

  validates_attachment_content_type :featured_image, content_type: /\Aimage\/.*\Z/
  validates_attachment_presence :featured_image
  
  validates :name, :description, presence: true
end
