# == Schema Information
#
# Table name: images
#
#  id                   :integer          not null, primary key
#  album_id             :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  content_file_name    :string
#  content_content_type :string
#  content_file_size    :integer
#  content_updated_at   :datetime
#

class Image < ActiveRecord::Base
  belongs_to :album

  has_attached_file :content, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :content, content_type: /\Aimage\/.*\Z/
end
