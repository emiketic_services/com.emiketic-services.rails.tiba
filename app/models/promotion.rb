# == Schema Information
#
# Table name: promotions
#
#  id                         :integer          not null, primary key
#  title                      :string
#  desc                       :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  landing_image_file_name    :string
#  landing_image_content_type :string
#  landing_image_file_size    :integer
#  landing_image_updated_at   :datetime
#  theater_image_file_name    :string
#  theater_image_content_type :string
#  theater_image_file_size    :integer
#  theater_image_updated_at   :datetime
#  featured                   :boolean
#  available                  :boolean          default(TRUE)
#  active_landing             :boolean          default(TRUE)
#  active_theater             :boolean          default(TRUE)
#  promo_link                 :string
#

class Promotion < ActiveRecord::Base
  has_one :project
  has_many :promo_quotes

  accepts_nested_attributes_for :project

  has_attached_file :landing_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :landing_image, content_type: /\Aimage\/.*\Z/

  has_attached_file :theater_image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :theater_image, content_type: /\Aimage\/.*\Z/

  validate :only_one_featured_promotion

  scope :featured, -> { where(featured: true) }
  scope :associated_to_project, -> { joins(:project).where(available: true) }

  def image
    self.theater_image || self.landing_image
  end

  protected

    def only_one_featured_promotion
      return unless featured?

      matches = Promotion.featured
      if persisted?
        matches = matches.where('id != ?', id)
      end
      if matches.exists?
        errors.add(:featured, 'cannot have another featured promotion')
      end
    end
end
