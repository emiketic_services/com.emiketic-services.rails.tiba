# == Schema Information
#
# Table name: social_networks
#
#  id         :integer          not null, primary key
#  name       :string
#  url        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  activated  :boolean          default(TRUE)
#

class SocialNetwork < ActiveRecord::Base
  validates_uniqueness_of :name
end
