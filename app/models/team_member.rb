# == Schema Information
#
# Table name: team_members
#
#  id                   :integer          not null, primary key
#  name                 :string
#  title                :string
#  email                :string
#  bio                  :text
#  linkedin             :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  picture_file_name    :string
#  picture_content_type :string
#  picture_file_size    :integer
#  picture_updated_at   :datetime
#  rank                 :integer
#

class TeamMember < ActiveRecord::Base
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"

  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  validates_attachment_presence :picture
  validates :name, :title, :email, :bio, :linkedin, :rank, presence: true

  scope :teams, -> { order(:rank) }
end
