# == Schema Information
#
# Table name: meta_configurations
#
#  id          :integer          not null, primary key
#  title       :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  project_id  :integer
#

class MetaConfiguration < ActiveRecord::Base
  belongs_to :project
  validates :project, uniqueness: {message: "Vous avez déjà associés un meta à ce project"}

end
