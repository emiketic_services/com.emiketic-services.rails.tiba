# == Schema Information
#
# Table name: filter_configurations
#
#  id         :integer          not null, primary key
#  max_surf   :integer
#  min_surf   :integer
#  min_price  :integer
#  max_price  :integer
#  min_room   :integer
#  max_room   :integer
#  nb_floor   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  project_id :integer
#

class FilterConfiguration < ActiveRecord::Base
  belongs_to :project
end
