ready =->
  $('#new_promo_quote').bind 'ajax:error', (event, data, status, xhr) ->
    clearFormErrors()
    displayErrors(data)



$(document).ready(ready)
$(document).on('page:load', ready)

displayErrors = (data)->
  $.each JSON.parse(data.responseText), (index) ->
    $("#promo_quote_"+index).after('<small class="error">'+index+' ne doit pas etre vide </small>')

clearFormErrors = ->
  $('#new_promo_quote').find('small').remove()
