ready = ->

  slideNext()
  openQuote()
  getProducts()
  mobileHover()
  convertUrlToEmbed()
  displayGallery()
  zoomPlan()
  setRibbon()

$(document).ready(ready)
$(document).on('page:load', ready)

slideNext = ->
	$('.mouse').on 'click', (e) ->
		e.preventDefault()
		ele = $(this).parent().parent().next().children().first()
		$('html, body').animate { scrollTop: $(ele).offset().top }, 1000

mobileHover = ->
  $('.project-details').on 'click', ->
    $(this).find('.project-controls').css("height","100%")

openQuote = ->
  $('#doc-project').on 'click',(e) ->
    e.preventDefault()
    $('.nav-action').click()
    $('#quote_project_id').val($(this).attr("data-project"))

getProducts = ->
  if $('body').hasClass('projects-show')
    projectId = $("#doc-project").attr("data-project")
    url = "/quotes/find_appartements"
    $.ajax(
      url: url
      data:
        project_id : projectId
      dataType: "script"
    )

convertUrlToEmbed = ->
	$('.video-url').each ->
		url = $(this).attr('href')
		videoId = url.split("watch?v=")[1]
		srcUrl = "https://www.youtube.com/embed/" + videoId
		$(this).closest('.video-holder').find('iframe').attr("src", srcUrl)


displayGallery = ->
  $('.gallery-link').on 'click', (e) ->
    current = $('.gallery-link').index($(this))
    $("ol").find("[data-orbit-slide='" + current + "']").click()

zoomPlan = ->
  $('.zoom-plan').on 'click', () ->
    $('.zoom-image').attr("src", $(this).data("plan-id"))

setRibbon = ->
    $('.ribbon').each ->
      cartoucheHeight = $(this).parent().parent().height() + 70
      cartoucheHeight += 'px'
      $(this).css('padding-left', cartoucheHeight)
