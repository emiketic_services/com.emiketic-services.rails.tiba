ready = ->
  $('#new_contact').on("ajax:error", (e, data, status, xhr) ->
    $("#new_contact").render_form_errors('contact', data.responseJSON)
  )

$(document).ready(ready)
$(document).on('page:load', ready)

$.fn.render_form_errors = (model_name, errors) ->
  form = this
  this.clear_form_errors()

  $.each(errors, (field, messages) ->
    input = form.find('input, select, textarea').filter(->
      name = $(this).attr('name')
      if name
        name.match(new RegExp(model_name + '\\[' + field + '\\(?')))
    input.after('<small class="error">' + $.map(messages, (m) -> m.charAt(0).toUpperCase() + m.slice(1)).join('<br />') + '</small>')
  )

$.fn.clear_form_errors = () ->
  this.find('small.error').remove()
