ready =->

  $('#new_newsletter').bind 'ajax:success', (event, data, status, xhr) ->
    $('#newsletter_email').val('')
    $(".alert-box").text("Votre demande d'inscription a été envoyé avec succès.")
    $(".alert-box" ).fadeIn( 100 ).delay( 3000 ).fadeOut( 500 );

  $('#new_newsletter').bind 'ajax:error', (event, jqxhr, settings, exception) ->
    errors = $.parseJSON(jqxhr.responseText)
    error_text =""
    $(errors.email).each ->
      error_text += @ + '<br />'

    $(".error-box").html(error_text)
    $(".error-box" ).fadeIn( 100 ).delay( 3000 ).fadeOut( 500 );


$(document).ready(ready)
$(document).on('page:load', ready)
