ready = ->
	_mobileNavig()
	_showLowBar()
	displaySearch()

	mq = window.matchMedia('(min-width: 500px)')

	if $('body').hasClass('projects-index') or (
		$('body').hasClass('projects-show') or $('body').hasClass('products-show') or $('body').hasClass('projects-promotions') or $('body').hasClass('projects-villas') or $('body').hasClass('projects-apartments') or $('body').hasClass('projects-commerces') or $('body').hasClass('projects-lotissements'))
		if mq.matches
			$('#low-bar').slideDown("fast")
			_stickyLowbar()
	else
		$(window).scroll ->
			if $(this).scrollTop() > 150
				if window.innerWidth > 800 && window.innerHeight > 600
					$('#low-bar').slideDown("fast")
				
				$('.top-bar').slideUp("fast")
			else
				$('.top-bar').slideDown("fast")

$(document).ready(ready)
$(document).on('page:load', ready)

_showLowBar = ->
	$('#project').hover ->
		$('#low-bar').slideDown("fast")

_stickyTopbar = ->
	$(window).scroll ->
		if $(this).scrollTop() > 150
			$('.top-bar').stop()

displaySearch = ->
	$('.search').on "click", ->
		if $(".project-form").is(":visible")
			$(".project-form form").submit()
			return
		$('.project-form').show 'slide', { direction: 'right' }, 300

_stickyLowbar = ->
	$(window).scroll ->
		if $(this).scrollTop() > 150
			$('.top-bar').slideUp("fast")
		else
			$('.top-bar').slideDown("fast")

_mobileNavig = ->
	$('#nav-icon').click ->
  	$(this).toggleClass 'open'
  	if $(this).hasClass 'open'
  		$('.mobile-top').show()
  	else
  		$('.mobile-top').hide()
