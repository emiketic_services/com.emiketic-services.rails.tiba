@Credit = React.createClass
  getInitialState: ->
    project: @props.data
    personal_apport: ''
    apport_percentage: ''
    taux_percentage: ''
    remboursement_period: 'Mensuelle'
    remboursement_duration: ''

  handleChange: (e) ->
    e.preventDefault()
    name = e.target.name
    @setState "#{ name }": e.target.value

  apportChange: (e) ->
    e.preventDefault()
    @setState taux_percentage: e.target.value

  getUnity: ->
    period = @state.remboursement_period
    if(period == "Mensuelle")
      return(
        React.DOM.span
          className: "unity"
          children: "Mois"
      )
    else
      return(
        React.DOM.span
          className: "unity"
          children: "Ans"
      )

  capitalize: (title) ->
    return title.charAt(0).toUpperCase() + title.slice(1)

  format: (amount) ->
    return  amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " DT"

  calculMensualRemb: ->
    period = @state.remboursement_period
    emprunt = @calculEmprunt()
    coeff = @state.taux_percentage/(12*100)

    if(period == "Mensuelle")
      month = @state.remboursement_duration
    else
      month = @state.remboursement_duration * 12

    if(@state.taux_percentage && @state.remboursement_duration )
      mensual = (emprunt * coeff)/(1-Math.pow((1+coeff), -month))
    else
      mensual = 0

    return Math.round(mensual)

  calculEmprunt : ->
    if(@state.personal_apport)
      return @state.project.price - @state.personal_apport
    else
      return 0

  render: ->
    React.DOM.form
      className: 'credit-form'

      React.DOM.div
        className: "title"
        React.DOM.h2
          children:  "Crédit pour " + @capitalize(@state.project.title)

      React.DOM.div
        className: "credit-card"
        React.DOM.span
          className: "card-title"
          children: "Prix du bien:"
        React.DOM.span
          className: "card-result price"
          children: @format(@state.project.price)

      React.DOM.div
        className:"row"
        React.DOM.div
          className: 'large-9 small-9 columns'
          React.DOM.label
            htmlFor: 'personal_apport'
            'Apport personnel*:'
          React.DOM.input
            type: 'number'
            className: 'form-input'
            placeholder: 'Personal Apport'
            name: 'personal_apport'
            value: @state.personal_apport
            onChange: @handleChange
        React.DOM.div
          className: 'large-3 small-3 columns'
          React.DOM.label
            htmlFor: 'percentage'
            "Taux d'intérêt (%) *"
          React.DOM.input
            type: 'number'
            className: 'form-input'
            name: "percentage"
            min:"7.5"
            max:"15"
            step: "0.1"
            onChange: @apportChange

      React.DOM.div
        className:"row"
        React.DOM.div
          className: 'large-12 small-12 columns'
          React.DOM.label
            htmlFor: 'remboursement_period'
            'Periode de remboursement*:'
          React.DOM.select
            name: 'remboursement_period'
            value: @state.remboursement_period
            onChange: @handleChange
            React.DOM.option
              key: '1'
              value: "Mensuelle"
              "Mensuelle"
            React.DOM.option
              key: '2'
              value: "Annuelle"
              "Annuelle"
      React.DOM.div
        className:"row"
        React.DOM.div
          className: 'large-10 small-10 columns'
          React.DOM.label
            htmlFor: 'remboursement_duration'
            'Durée de remboursement*:'
          React.DOM.input
            type: 'number'
            className: 'form-input'
            placeholder: 'Duree du Remboursement'
            name: 'remboursement_duration'
            value: @state.remboursement_duration
            onChange: @handleChange
        React.DOM.div
          className: 'large-2 small-2 columns'
          @getUnity()
      React.DOM.div
        className: "credit-card"
        React.DOM.span
          className: "card-title"
          children: "Le remboursement mensuel estimé:"
        React.DOM.span
          className: "card-result"
          children: "#{@calculMensualRemb()} DT "
      React.DOM.div
        className: "credit-card"
        React.DOM.span
          className: "card-title"
          children: "Montant de l'emprunt:"
        React.DOM.span
          className: "card-result"
          children: "#{@calculEmprunt()} DT "
