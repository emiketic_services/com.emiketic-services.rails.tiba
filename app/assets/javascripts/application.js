// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.cookie
//= require jquery_ujs
//= require jquery.ui.all
//= require foundation
//= require turbolinks
//= require underscore
//= require gmaps/google
//= require classie
//= require cssParser
//= require modals
//= require ckeditor/init
//= require react
//= require react_ujs
//= require components
//= require_tree .


$(function(){
  'use strict';
  $.cookie('presence','1');

  // Show/Hide featured promotion
  $(window).scroll(function() {
    if( $.cookie("presence")!= '0'){
      if ($(this).scrollTop() > 50) {
        $('#featured-promotion').stop().animate({ right: '0px' });
      } else {
        $('#featured-promotion').stop().animate({ right: '-315px' });
      }
    }

  });

  $(document).foundation({
    orbit: {
      animation: 'fade',
      timer_speed: 2000,
      pause_on_hover: false,
      animation_speed: 1000,
      navigation_arrows: false,
      slide_number: false,
      timer_paused_class: 'slider-pause',
      bullets: false,
      timer: true
    }
  });
});
