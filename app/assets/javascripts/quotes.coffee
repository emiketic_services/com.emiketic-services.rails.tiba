ready = ->

  getAppartementByProject()
  rotateClose()

  $('#new_quote').on("ajax:success", (e, data, status, xhr) ->
    $('#new_quote').clear_form_errors()
    $("#new_quote")[0].reset()
    $('#close').click()
    $(".alert-box").text("Votre demande de dossier a été envoyer avec succés")
    $(".alert-box").fadeIn( 400 ).delay( 3000 ).fadeOut( 400 )
  ).on("ajax:error", (e, data, status, xhr) ->
    $("#new_quote").render_form_errors('quote', data.responseJSON)
  )

  $('#close').on 'click', ->
    $('#new_quote').clear_form_errors()
    $('body').css 'overflow-y': 'scroll'

  $('.overlay').on 'click', ->
    $("#close").click()
    $('body').css 'overflow-y': 'scroll'


$(document).ready(ready)
$(document).on('page:load', ready)

rotateClose = ->
  $('.nav-action').click ->
    $('body').css 'overflow-y': 'hidden'
    translateValue = $(window).width() - ($('#offcanvas-menu').offset().left + $('#offcanvas-menu').outerWidth())

getAppartementByProject = ->
  $('#quote_project_id').change ->
    projectId = $(this).val();
    if projectId != ''
      url = "/quotes/find_appartements"
      $.ajax(
        url: url
        data:
          project_id : projectId
        dataType: "script"
      )

$.fn.render_form_errors = (model_name, errors) ->
  form = this
  this.clear_form_errors()

  $.each(errors, (field, messages) ->
    input = form.find('input, select, textarea').filter(->
      name = $(this).attr('name')
      if name
        name.match(new RegExp(model_name + '\\[' + field + '\\(?')))
    input.after('<small class="error">' + $.map(messages, (m) -> m.charAt(0).toUpperCase() + m.slice(1)).join('<br />') + '</small>')
  )

$.fn.clear_form_errors = () ->
  this.find('small.error').remove()
