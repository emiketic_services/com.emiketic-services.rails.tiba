ready = ->
  openQuote()
  getProductByProject()
  zoomProductPlan()

$(document).ready(ready)
$(document).on('page:load', ready)


openQuote = ->
  $('#doc-product').on 'click',  ->
    $('#quote_project_id').val($(this).attr("data-project"))
    $('#quote_product_id').val($(this).attr("data-product"))
    $('.nav-action').click()

getProductByProject = ->
  if $('body').hasClass('products-show')
    projectId = $("#doc-product").attr("data-project")
    url = "/quotes/find_appartements"
    $.ajax(
      url: url
      data:
        project_id : projectId
      dataType: "script"
    )
zoomProductPlan = ->
  $('.zoom-plan').on 'click', () ->
    $('.zoom-image').attr("src", $(this).data("plan-id"))
