ready = ->
	removePopup()
	getVideosUrl()
	startOrbitSlider()
	scrollDown()
	$(".flash-box").fadeIn( 400 ).delay( 3000 ).fadeOut( 400 )

$(document).ready(ready)
$(document).on('page:load', ready)

removePopup = ->
	$('#hover').click ->
    $(this).fadeOut()
    $('#popup-promotion').fadeOut()

  $('#close-promotion').click ->
    $('#hover').fadeOut()
    $('#popup-promotion').fadeOut()

startOrbitSlider = ->
	$(document).foundation orbit:
	  animation: 'fade'
	  timer_speed: 2000
	  pause_on_hover: false
	  animation_speed: 1000
	  navigation_arrows: false
	  slide_number: false
	  timer_paused_class: 'slider-pause'
	  bullets: false
	  timer: true

scrollDown = ->
		$('#scroll-mouse').on 'click', (e) ->
			e.preventDefault()
			ele = $(this).parent().next()
			$('html, body').animate { scrollTop: $(ele).offset().top }, 1000

getVideoId = (url) ->
	videoId = url.split("watch?v=")[1]
	return videoId

getVideosUrl = ->
	$('.video-url').each ->
		url = $(this).attr('href')
		videoId = getVideoId(url)
		srcUrl = "https://www.youtube.com/embed/" + videoId
		$(this).closest('.video-holder').find('iframe').attr("src", srcUrl)
