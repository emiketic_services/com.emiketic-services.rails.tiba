ready = ->
  rangeSurfInit()
  rangeRoomInit()
  rangePriceInit()
  floorSetChange()

$(document).ready(ready)
$(document).on('page:load', ready)

rangeSurfInit = ->
	$('.slider-surf').slider
	  range: true
	  min: 100
	  max: 600
	  values: [100 , 600]
	  slide: (event, ui) ->
	    $('.min-surf').text ui.values[0]
	    $('.max-surf').text ui.values[1]
    change: ->
      minVal = $('.min-surf').text()
      maxVal = $('.max-surf').text()
      $('#q_superficie_gteq').val(minVal)
      $('#q_superficie_lteq').val(maxVal)
      $('.product_search').submit()


rangeRoomInit = ->
	$('.slider-rooms').slider
	  range: true
	  min: 1
	  max: 10
	  values: [1 , 10]
	  slide: (event, ui) ->
	    $('.min-rooms').text ui.values[0]
	    $('.max-rooms').text ui.values[1]

    change: ->
      minRoom = $('.min-rooms').text()
      maxRoom = $('.max-rooms').text()
      $('#q_configuration_gteq').val(minRoom)
      $('#q_configuration_lteq').val(maxRoom)
      $('.product_search').submit()

rangePriceInit = ->
	$('.slider-price').slider
	  range: true
	  min: 10000
	  max: 500000
	  values: [10000 , 500000]
	  slide: (event, ui) ->
	    $('.min-price').text ui.values[0]
	    $('.max-price').text ui.values[1]

    change: ->
      minPrice = $('.min-price').text()
      maxPrice = $('.max-price').text()
      $('#q_price_gteq').val(minPrice)
      $('#q_price_lteq').val(maxPrice)
      $('.product_search').submit()

floorSetChange = ->
  $('.num-floor').bind 'keyup mouseup',  ->
    if $(this).val() <= "0"
    	$(this).val("")

    $('#q_floor_eq').val($(this).val())
    $('.product_search').submit()
