# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  email      :string
#  country    :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  message    :text
#  ville      :string
#

module ContactsHelper
end
