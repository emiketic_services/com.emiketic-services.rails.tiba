# == Schema Information
#
# Table name: promotions
#
#  id                         :integer          not null, primary key
#  title                      :string
#  desc                       :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  landing_image_file_name    :string
#  landing_image_content_type :string
#  landing_image_file_size    :integer
#  landing_image_updated_at   :datetime
#  theater_image_file_name    :string
#  theater_image_content_type :string
#  theater_image_file_size    :integer
#  theater_image_updated_at   :datetime
#  featured                   :boolean
#  available                  :boolean          default(TRUE)
#  active_landing             :boolean          default(TRUE)
#  active_theater             :boolean          default(TRUE)
#  promo_link                 :string
#

module PromotionsHelper
  def link_for_promotion(promotion)
    if promotion.promo_link.present?
      link_to promotion.promo_link, target: '_blank' do
        yield
      end
    else
      link_to promotion_path(promotion) do
        yield
      end
    end
  end
end
