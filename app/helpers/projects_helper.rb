# == Schema Information
#
# Table name: projects
#
#  id                          :integer          not null, primary key
#  title                       :string
#  overview                    :text
#  description                 :text
#  location                    :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  plan_image_file_name        :string
#  plan_image_content_type     :string
#  plan_image_file_size        :integer
#  plan_image_updated_at       :datetime
#  category                    :string
#  slug                        :string
#  latitude                    :float
#  longitude                   :float
#  promotion_id                :integer
#  featured_image_file_name    :string
#  featured_image_content_type :string
#  featured_image_file_size    :integer
#  featured_image_updated_at   :datetime
#  video_url                   :string
#  map_url                     :string
#  rank                        :integer
#  available                   :boolean          default(TRUE)
#  plan_pdf_file_name          :string
#  plan_pdf_content_type       :string
#  plan_pdf_file_size          :integer
#  plan_pdf_updated_at         :datetime
#  brochure_pdf_file_name      :string
#  brochure_pdf_content_type   :string
#  brochure_pdf_file_size      :integer
#  brochure_pdf_updated_at     :datetime
#  max_surf                    :integer
#  min_surf                    :integer
#  min_price                   :integer
#  max_price                   :integer
#  min_room                    :integer
#  max_room                    :integer
#  nb_floor                    :integer
#  sold                        :boolean          default(FALSE)
#  show_area                   :boolean          default(TRUE)
#  show_price                  :boolean          default(TRUE)
#  show_room                   :boolean          default(TRUE)
#  show_floor                  :boolean          default(TRUE)
#  price                       :integer
#  vcard_file_name             :string
#  vcard_content_type          :string
#  vcard_file_size             :integer
#  vcard_updated_at            :datetime
#

module ProjectsHelper
  def grid_size_for(projects)
    return projects.size % 3 == 0 ? '3' : '2'
  end

  def gallery_size_for(photos)
    if photos.size <= 2
      return '2'
    else
      return '3'
    end
  end

  def set_filter(value, default)
   value.nil? ? default : value
  end

  def span_value_for(project)
    shown_filters = [project.show_room, project.show_floor, project.show_area, project.show_price].select do |element|
        element == true
    end

    return 12 / shown_filters.size
  end

  def plan_hint(obj)
    if obj.plan_pdf?
      content_tag :span do
        concat('PDF actuel:')
        concat(link_to(obj.plan_pdf_file_name, obj.plan_pdf.url, target: '_blank'))
      end
    end
  end

  def brochure_hint(obj)
    if obj.brochure_pdf?
      content_tag :span do
        concat('PDF actuel:')
        concat(link_to(obj.brochure_pdf_file_name, obj.brochure_pdf.url, target: '_blank'))
      end
    end
  end
end
