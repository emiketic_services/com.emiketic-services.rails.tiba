module ApplicationHelper

  def vcard_hint(obj)
    if obj.vcard?
      content_tag :span do
        concat('Vcard actuel:')
        concat(link_to(obj.vcard_file_name, obj.vcard.url, target: '_blank'))
      end
    end
  end

  ## Create Helpers to set title and meta_tags in Rails View
  def title(text)
    content_for :title, text
  end
  def meta_tag(tag, text)
    content_for :"meta_#{tag}", text
  end
  def yield_meta_tag(tag, default_text='')
    content_for?(:"meta_#{tag}") ? content_for(:"meta_#{tag}") : default_text
  end
end
