# == Schema Information
#
# Table name: quotes
#
#  id          :integer          not null, primary key
#  project_id  :integer
#  first_name  :string
#  last_name   :string
#  email       :string
#  phone       :string
#  avenue      :string
#  code_postal :string
#  loc         :string
#  pays        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  product_id  :integer
#

class QuotesController < ApplicationController

  def create
    @quote = Quote.new(quote_params)
    if @quote.save
      QuoteMailer.forward_quote(@quote).deliver_later
      render json: {}, status: 200
    else
      render json: @quote.errors.messages, status: 422
    end
  end

  def find_appartements
    @products = Product.where project_id: params[:project_id], sold: false

    respond_to do |format|
      format.js {}
    end
  end

  private
    def quote_params
      params.require(:quote).permit(:project_id, :product_id, :first_name, :last_name, :email,
        :phone, :avenue, :code_postal, :loc, :pays)
    end
end
