# == Schema Information
#
# Table name: products
#
#  id                           :integer          not null, primary key
#  name                         :string
#  overview                     :text
#  superficie                   :integer
#  configuration                :integer
#  floor                        :integer
#  project_id                   :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  sold                         :boolean          default(FALSE), not null
#  slug                         :string
#  price                        :decimal(, )
#  plan_image_file_name         :string
#  plan_image_content_type      :string
#  plan_image_file_size         :integer
#  plan_image_updated_at        :datetime
#  description_technique        :text
#  document_pdf_file_name       :string
#  document_pdf_content_type    :string
#  document_pdf_file_size       :integer
#  document_pdf_updated_at      :datetime
#  rank                         :integer
#  picture_file_name            :string
#  picture_content_type         :string
#  picture_file_size            :integer
#  picture_updated_at           :datetime
#  related_picture_file_name    :string
#  related_picture_content_type :string
#  related_picture_file_size    :integer
#  related_picture_updated_at   :datetime
#  product_plan_file_name       :string
#  product_plan_content_type    :string
#  product_plan_file_size       :integer
#  product_plan_updated_at      :datetime
#

class ProductsController < ApplicationController

  def show
  	@product = Product.friendly.find(params[:id])
    session[:related_project_page] = request.env['HTTP_REFERER'] || project_path(@product.project)

  end

  def find_appartements
    @product_id = Product.where project_id: params[:project_id]
    respond_to do |format|
      format.js {}
    end
    @product = Product.friendly.find(params[:id])
  end

end
