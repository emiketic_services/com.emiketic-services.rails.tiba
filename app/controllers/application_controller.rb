class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_search unless controller_name == 'projects' && action_name == 'index'
  before_action :set_quotes, :set_phones, :set_newsletters, :get_meta_configuration,
                :set_social_networks, :set_videos_home, :set_album_home


  private
    def set_search
      @q = Project.ransack(params[:q])
      @projects = @q.result(distinct: true)
    end

    def set_quotes
      @quote = Quote.new
      @projects = Project.not_sold
    end

    def set_newsletters
      @newsletter = Newsletter.new
    end

    def set_social_networks
      @available_social_networks = SocialNetwork.all
    end

    def set_videos_home
      @available_videos = Videohome.all
    end

    def set_album_home
      @home_album = Album.all
    end

    def set_phones
      @phones = Phone.all
    end

    def get_meta_configuration
      @meta_configurations = MetaConfiguration.all
    end

end
