# == Schema Information
#
# Table name: credit_simulators
#
#  id                     :integer          not null, primary key
#  personal_apport        :integer
#  remboursement_period   :string
#  remboursement_duration :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class CreditSimulatorsController < ApplicationController
  def index
    @project = Project.friendly.find(params[:project_id])
  end
end
