# == Schema Information
#
# Table name: promo_quotes
#
#  id           :integer          not null, primary key
#  promotion_id :integer
#  first_name   :string
#  last_name    :string
#  email        :string
#  phone        :string
#  code_postal  :string
#  loc          :string
#  pays         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  ville        :string
#

class PromoQuotesController < ApplicationController

  def create
    @promotion = Promotion.find(params[:promotion_id])
    @promo_quote = @promotion.promo_quotes.build(promo_quote_params)
    if @promo_quote.save
      flash[:notice] = "Votre demande d'une devis de promotion a été envoyer avec succés"
      render js: "window.location='#{root_path}'"

    else
      render json: @promo_quote.errors.messages, status: 422
    end
  end

  private

    def promo_quote_params
      params.require(:promo_quote).permit(:promotion, :first_name, :last_name, :email,
        :phone, :avenue, :code_postal, :loc, :pays)
    end
end
