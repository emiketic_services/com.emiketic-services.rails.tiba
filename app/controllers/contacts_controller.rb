# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  email      :string
#  country    :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  message    :text
#  ville      :string
#

class ContactsController < ApplicationController

  def create
    @contact = Contact.new(contact_params)


    if @contact.save
      ContactMailer.forward_email(@contact).deliver_later
      flash[:notice] = "Votre demande d'information a été envoyer avec succés"
      render js: "window.location='#{root_path}'"
    else
      render json: @contact.errors.messages, status: 422
    end

  end

  private
    def contact_params
      params.require(:contact).permit(:first_name, :last_name, :email, :country, :phone, :message)
    end
end
