class StaticPagesController < ApplicationController
  def home
    @promotions_landing = Promotion.associated_to_project
                                   .where.not(landing_image_file_name: nil, active_landing: false)
    @promotions_theater = Promotion.associated_to_project
                                   .where.not(theater_image_file_name: nil, active_theater: false)
    @home_slider = Slideshow.all
  end

  def contact
    @contact = Contact.new
  end

  def about
    @team_members = TeamMember.teams
  end
end
