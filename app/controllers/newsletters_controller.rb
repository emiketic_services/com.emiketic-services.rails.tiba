# == Schema Information
#
# Table name: newsletters
#
#  id         :integer          not null, primary key
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NewslettersController < ApplicationController
  def create
    @newsletter = Newsletter.new(newsletter_params)

    if @newsletter.save
      NewsletterMailer.forward_newsletter(@newsletter).deliver_later
      redirect_to root_url
    else
      render json: @newsletter.errors.messages, status: 422
    end

  end

  private
    def newsletter_params
      params.require(:newsletter).permit(:email)
    end
end
