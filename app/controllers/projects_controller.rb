# == Schema Information
#
# Table name: projects
#
#  id                          :integer          not null, primary key
#  title                       :string
#  overview                    :text
#  description                 :text
#  location                    :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  plan_image_file_name        :string
#  plan_image_content_type     :string
#  plan_image_file_size        :integer
#  plan_image_updated_at       :datetime
#  category                    :string
#  slug                        :string
#  latitude                    :float
#  longitude                   :float
#  promotion_id                :integer
#  featured_image_file_name    :string
#  featured_image_content_type :string
#  featured_image_file_size    :integer
#  featured_image_updated_at   :datetime
#  video_url                   :string
#  map_url                     :string
#  rank                        :integer
#  available                   :boolean          default(TRUE)
#  plan_pdf_file_name          :string
#  plan_pdf_content_type       :string
#  plan_pdf_file_size          :integer
#  plan_pdf_updated_at         :datetime
#  brochure_pdf_file_name      :string
#  brochure_pdf_content_type   :string
#  brochure_pdf_file_size      :integer
#  brochure_pdf_updated_at     :datetime
#  max_surf                    :integer
#  min_surf                    :integer
#  min_price                   :integer
#  max_price                   :integer
#  min_room                    :integer
#  max_room                    :integer
#  nb_floor                    :integer
#  sold                        :boolean          default(FALSE)
#  show_area                   :boolean          default(TRUE)
#  show_price                  :boolean          default(TRUE)
#  show_room                   :boolean          default(TRUE)
#  show_floor                  :boolean          default(TRUE)
#  price                       :integer
#  vcard_file_name             :string
#  vcard_content_type          :string
#  vcard_file_size             :integer
#  vcard_updated_at            :datetime
#

class ProjectsController < ApplicationController
  def index
    @q = Project.ransack(params[:q])
    @projects = @q.result(distinct: true).available.project_order
    @projects_header = ProjectsHeader.first
  end

  def get_urls
    @project = Project.find(params[:project_id])
    respond_to do |format|
      format.js {}
    end
  end

  def plan
    @project = Project.friendly.find(params[:id])
    respond_to do |format|
     format.pdf do
       render pdf: "#{@project.title}-plan", # pdf will download as my_pdf.pdf
       layout: 'pdf'
     end
   end
  end

  def show
  	@project = Project.friendly.find(params[:id])
    @gallery_photos = @project.gallery_photos
    @project_videos = @project.project_videos
    @project_meta = @project.meta_configurations
    @search = @project.products.ransack(params[:q])
    @products = @search.result(distinct: true).product_order

    session[:last_project_page] = request.env['HTTP_REFERER'] || projects_path

    respond_to do |format|
      format.html
      format.js {}
    end
  end

  def find_appartements
    @product = Product.where project_id: params[:project_id]
    respond_to do |format|
      format.js {}
    end
  end

  def promotions
    @projects = Project.promotions.project_order
  end

  def villas
    @projects = Project.villas.available.project_order
    @category_header = Testimonial.villas.first
  end

  def apartments
    @projects = Project.apartments.available.project_order
    @category_header = Testimonial.apartments.first
  end

  def commerces
    @projects = Project.commerces.available.project_order
    @category_header = Testimonial.commerces.first
  end

  def lotissements
    @projects = Project.lotissements.available.project_order
    @category_header = Testimonial.lotissements.first
  end


end
