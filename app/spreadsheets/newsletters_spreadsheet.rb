class NewslettersSpreadsheet
  attr_accessor :newsletters

  def initialize newsletters
    @newsletters = newsletters
  end

  def generate_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: 'Newsletters'

    create_body sheet

    data_to_send = StringIO.new
    book.write data_to_send
    data_to_send.string
  end

  def create_body sheet
    # Header row with a specific format
    sheet.row(0).concat %w{Email}
    sheet.row(0).default_format = Spreadsheet::Format.new weight: :bold

    row_index = 1
    newsletters.each do |newsletter|
      sheet.row(row_index).concat [newsletter.email]
      row_index += 1
    end
  end
end
