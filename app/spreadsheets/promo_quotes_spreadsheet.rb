class PromoQuotesSpreadsheet
  attr_accessor :promo_quotes_list
  def initialize promo_quotes_list
    @promo_quotes_list = promo_quotes_list
  end

  def generate_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: 'promo_quotes'

    create_body sheet

    data_to_send = StringIO.new
    book.write data_to_send
    data_to_send.string
  end

  def create_body sheet
    # Header row with a specific format
    sheet.row(0).concat %w{FirstName LastName Email Phone Address CodePostal Country Ville Promotion}
    sheet.row(0).default_format = Spreadsheet::Format.new weight: :bold

    row_index = 1
    promo_quotes_list.each do |promo_quote|
      sheet.row(row_index).concat [
        promo_quote.first_name,
        promo_quote.last_name,
        promo_quote.email,
        promo_quote.phone,
        promo_quote.loc,
        promo_quote.code_postal,
        promo_quote.pays,
        promo_quote.ville,
        promo_quote.promotion.title
      ]
      row_index += 1
    end
  end
end
