class QuotesSpreadsheet
  attr_accessor :quotes_list

  def initialize quotes_list
    @quotes_list = quotes_list
  end

  def generate_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: 'quotes'

    create_body sheet

    data_to_send = StringIO.new
    book.write data_to_send
    data_to_send.string
  end

  def create_body sheet
    # Header row with a specific format
    sheet.row(0).concat %w{FirstName LastName Email Phone Avenue Address CodePostal Country Project }
    sheet.row(0).default_format = Spreadsheet::Format.new weight: :bold

    row_index = 1
    quotes_list.each do |quote|
      sheet.row(row_index).concat [
        quote.first_name,
        quote.last_name,
        quote.email,
        quote.phone,
        quote.avenue,
        quote.loc,
        quote.code_postal,
        quote.pays,
        quote.project.title
      ]
      row_index += 1
    end
  end
end
