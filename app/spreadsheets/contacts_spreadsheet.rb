class ContactsSpreadsheet
  attr_accessor :contact_list

  def initialize contact_list
    @contact_list = contact_list
  end

  def generate_xls
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: 'contacts'

    create_body sheet

    data_to_send = StringIO.new
    book.write data_to_send
    data_to_send.string
  end

  def create_body sheet
    # Header row with a specific format
    sheet.row(0).concat %w{FirstName LastName Email Country Phone Message }
    sheet.row(0).default_format = Spreadsheet::Format.new weight: :bold

    row_index = 1
    contact_list.each do |contact|
      sheet.row(row_index).concat [contact.first_name, contact.last_name, contact.email, contact.country, contact.phone, contact.message]
      row_index += 1
    end
  end
end
