ActiveAdmin.register TeamMember do
  menu label: "Equipes"

  permit_params :name, :title, :email, :bio, :linkedin, :picture, :rank

  index do
    selectable_column

    column :picture do |team_member|
      image_tag team_member.picture.url(:thumb)
    end
    column :title
    column :name
    column :email
    column :linkedin
    column :bio
    column :rank
    actions
  end

  show do
    attributes_table do
      row :picture do |team_member|
        image_tag team_member.picture.url(:thumb)
      end
      row :title
      row :name
      row :email
      row :linkedin
      row :bio
      row :rank
    end
  end

  form do |f|
    inputs 'General' do
      input :title
      input :name
      input :email
      input :linkedin
      input :bio
      input :rank
    end

    inputs 'Attachment' do
      input :picture
    end

    f.actions
  end
end
