ActiveAdmin.register Quote do
  menu label: "Devis"
  actions :all, except: [:new]
  index download_links: [:xls]
  controller do
    def index
      @quotes_list= Quote.all
      index! do |format|
        format.xls {
          spreadsheet = QuotesSpreadsheet.new @quotes_list
          send_data spreadsheet.generate_xls, filename: 'tiba-devis.xls'
        }
      end
    end
  end
end
