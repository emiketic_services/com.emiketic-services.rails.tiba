ActiveAdmin.register Testimonial do

  permit_params :description, :name, :image,
    :category, :avatar, :hide
  index do
    selectable_column

    column :hide
    column :image do |testimonial|
      image_tag testimonial.image.url(:thumb)
    end
    column :description
    column :name
    column :category
    actions
  end

  show do
    attributes_table do
      row :hide
      row :image do |testimonial|
        image_tag testimonial.image.url(:thumb)
      end
      row :description
      row :category
      row :name
    end
  end

  form do |f|
    inputs 'General' do
      input :hide
      input :category,
            as: :select,
            collection: Testimonial::PROJECT_CATEGORIES,
            include_blank: true
      input :description
      input :name
      input :avatar
    end
    inputs 'Testimonial background' do
      input :image
    end


    f.actions
  end


end
