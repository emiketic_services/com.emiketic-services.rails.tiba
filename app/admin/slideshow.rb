ActiveAdmin.register Slideshow do
  menu label: "Slideshow"
  actions :all, except: [:new, :destroy]

  permit_params slides_attributes: [:id, :image, :project_id, :_destroy]

  form do |f|
    f.has_many :slides do |slide|
      slide.input :image, as: :file, hint: image_tag(slide.object.image.url(:medium))

      slide.input :project
      if (slide.present?)
        slide.input :_destroy, :as=> :boolean, :label => 'Remove Slide '
      end
    end

    f.actions
  end

  controller do
    def index
      redirect_to edit_resource_url(Slideshow.first)
    end
  end
end
