ActiveAdmin.register Phone do
  menu label: "Compagnie Informations"
  actions :all, except: [:new, :destroy]

  permit_params :vcard, phone_numbers_attributes: [:id, :number, :_destroy]

  controller do
    def index
      redirect_to edit_resource_url(Phone.first)
    end
  end

  form :title => 'Compagnie Informations' do |f|
    inputs 'Phones' do
      f.has_many :phone_numbers do |phone_number|
        phone_number.input :number
        if (phone_number.object.number.present?)
          phone_number.input :_destroy, :as=> :boolean, :label => 'Remove phone number'
        end
      end
    end

    inputs 'Vcard Attachment' do
      input :vcard, hint: vcard_hint(f.object)
    end

    f.actions
  end


end
