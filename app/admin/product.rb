ActiveAdmin.register Product do
  menu label: "Produits"

  permit_params :project_id, :name,:overview, :superficie, :configuration, :related_picture,
   :floor, :sold, :price, :plan_image, :rank, :description_technique, :document_pdf

  index do
    selectable_column

    column :project
    column :name
    column :overview
    column :document_pdf do |product|
      product.document_pdf? ? link_to(product.document_pdf_file_name, product.document_pdf.url, target: '_blank') : ''
    end
    column :superficie
    column :configuration
    column :floor
    column :sold
    column :price
    actions
  end

  show do
    attributes_table do
      row :project
      row :name
      row :overview
      row :document_pdf do
        product.document_pdf? ? link_to(product.document_pdf_file_name, product.document_pdf.url, target: '_blank') : ''
      end
      row :superficie
      row :configuration
      row :floor
      row :sold
      row :price
    end
  end

  form do |f|
    inputs 'General' do
      input :project
      input :rank
      input :name
      input :overview, as: :ckeditor
      input :description_technique
      input :related_picture, as: :file, hint: image_tag(f.object.related_picture.url(:thumb))
      input :document_pdf, hint: document_hint(f.object)
      input :superficie
      input :configuration, :as => :select, collection: Product::APPARTEMENT_CONFIG
      input :floor
      input :price
      input :sold, as: :boolean
      input :plan_image, as: :file, hint: image_tag(f.object.plan_image.url(:thumb))
    end


    f.actions
  end


end
