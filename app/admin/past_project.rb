ActiveAdmin.register PastProject do
  menu label: "Références"

  permit_params :name, :description, :featured_image

  index do
    selectable_column

    column :featured_image do |past_project|
      image_tag past_project.featured_image.url(:thumb)
    end
    column :name
    column :description
    actions
  end

  show do
    attributes_table do
      row :featured_image do |past_project|
        image_tag past_project.featured_image.url(:thumb)
      end
      row :name
      row :description
    end
  end

  form do |f|
    inputs 'General' do
      input :name
      input :description
    end

    inputs 'Attachment' do
      input :featured_image
    end

    f.actions
  end
end
