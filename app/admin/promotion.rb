ActiveAdmin.register Promotion do
  menu label: "Promotions"

  permit_params :title, :desc, :landing_image, :theater_image, :featured,
                :available, :active_landing, :active_theater, :promo_link
  index do
    selectable_column
    column :available
    column :featured
    column :title
    column :desc

    column :landing_image do |promotion|
      image_tag promotion.landing_image.url(:thumb)
    end
    column :theater_image do |promotion|
      image_tag promotion.theater_image.url(:thumb)
    end
    actions
  end

  show do
    attributes_table do
      row :available
      row :featured
      row :title
      row :desc
      row :landing_image do |promotion|
        image_tag promotion.landing_image.url(:thumb)
      end
      row :theater_image do |promotion|
        image_tag promotion.theater_image.url(:thumb)
      end

    end
  end

  form do |f|
    inputs 'General' do
      input :available
      input :featured
      input :title
      input :promo_link
      input :desc
    end

    inputs 'Landing Image' do
      input :landing_image
      input :active_landing, label: "active"
    end
    inputs 'Theater Image'do
      input :theater_image
      input :active_theater, label: "active"
    end

    f.actions
  end


end
