ActiveAdmin.register Videohome do

  menu label: "Video Home"

  actions :all, except: [:new, :destroy]
  permit_params :title, videos_attributes: [ :id, :url, :_destroy]

  form do |f|
    f.inputs "title" do
      f.input :title
    end

    f.has_many :videos do |video|
      video.input :url
      if (video.object.url.present?)
        video.input :_destroy, :as=> :boolean, :label => 'Remove Video'
      end
    end
    f.actions
  end

  controller do
    def index
      redirect_to edit_resource_url(Videohome.first)
    end
  end

end
