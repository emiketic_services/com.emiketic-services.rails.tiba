ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Projets récents" do
          ul do
            Project.last(5).map do |project|
              li link_to(project.title.titleize, admin_project_path(project))
            end
          end
        end
      end

      column do
        panel "Inscription Newsletter récentes" do
          ul do
            Newsletter.last(5).map do |newsletter|
              li newsletter.email
            end
          end
        end
      end
    end

    columns do

      column do
        panel "Demande de devis récentes" do
          ul do
            Quote.last(5).map do |quote|
              li link_to(quote.email, admin_quote_path(quote))
            end
          end
        end
      end

      column do
        panel "Demande de devis récentes (Promotions)" do
          ul do
            PromoQuote.last(5).map do |quote|
              li link_to(quote.email, admin_quote_path(quote))
            end
          end
        end
      end
    end
  end
end
