ActiveAdmin.register Album do
  menu label: "Home Album"
  actions :all, except: [:new, :destroy]

  permit_params :name, images_attributes: [:id, :content, :_destroy]

  form do |f|
    inputs 'General' do
      input :name
    end

    f.has_many :images do |image|
      image.input :content, as: :file, hint: image_tag(image.object.content.url(:medium))
      if (image.object.content.present?)
        image.input :_destroy, :as=> :boolean, :label => 'Remove Image'
      end
    end

    f.actions
  end

  controller do
    def index
      redirect_to edit_resource_url(Album.first)
    end
  end

end
