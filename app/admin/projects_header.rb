ActiveAdmin.register ProjectsHeader do

  menu label: "Contenu (rechercher un bien)"
  actions :all, except: [:new]

  permit_params :image, :content

  form do |f|
    inputs 'General' do
      input :content
      input :image
    end

    f.actions
  end

  controller do
    def index
      redirect_to edit_resource_url(ProjectsHeader.first)
    end
  end

end
