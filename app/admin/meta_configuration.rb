ActiveAdmin.register MetaConfiguration, as: "SEO Meta" do

 permit_params :project_id, :title, :description

 index do
   selectable_column

   column :project
   column :title
   column :description
   actions
 end

 show do
   attributes_table do
     row :project
     row :title
     row :description
   end
 end

 form do |f|
   inputs 'General' do
     input :project
     input :title
     input :description
   end
   f.actions
 end


end
