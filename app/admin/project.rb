ActiveAdmin.register Project do
  menu label: "Projets"

  permit_params :title, :overview, :description, :price, :location, :plan_image,
    :category, :promotion_id, :featured_image, :video_url,
    :map_url, :rank, :available, :plan_pdf, :sold,
    :brochure_pdf, :max_surf, :max_room, :max_price, :nb_floor,
    :min_surf, :min_room, :min_price, :show_area, :show_room,
    :show_price, :show_floor,
    project_videos_attributes: [:id, :video_url, :_destroy],
    gallery_photos_attributes: [:id, :photo, :_destroy]

  index do
    selectable_column
    column :sold
    column :plan_image do |project|
      image_tag project.plan_image.url(:thumb)
    end
    column :project_video do |video|
      div video.video_url
    end
    column :title
    column :overview
    column :description
    column :location
    column :category
    column :promotion
    column :rank

    actions
  end

  show do
    attributes_table do
      row :featured_image do |project|
        image_tag project.featured_image.url(:thumb)
      end
      row :sold
      row :title
      row :overview
      row :description
      row :location
      row :latitude
      row :longitude
      row :category
      row :promotion
      row :rank
    end
  end

  form do |f|
    inputs 'General' do
      input :available
      input :category,
            as: :select,
            collection: Project::PROJECT_CATEGORIES,
            include_blank: true
      input :promotion,
            as: :select,
            include_blank: true
      input :sold
      input :title
      input :overview
      input :description, as: :ckeditor
      input :price
      input :brochure_pdf, hint: brochure_hint(f.object)
      input :location
      input :map_url
      input :rank

    end


    inputs 'Filter Configuration' do

      columns do
        column do
          input :max_surf, label: "Max area"
          input :max_price, label: "Max price"
          input :max_room, label: "Max room"
          input :nb_floor, label: "Number of floor"
        end
        column do
          input :min_surf, label: "Min area"
          input :min_price, label: "Min price"
          input :min_room, label: "Min room"
        end
        column do
          input :show_area
          input :show_price
          input :show_room
          input :show_floor
        end
      end

    end

    inputs 'Video 360°' do
      input :video_url
    end

    inputs 'Attachment' do
      input :plan_image, as: :file, hint: image_tag(object.plan_image.url(:thumb))
      input :plan_pdf, hint: plan_hint(f.object)
      input :featured_image, as: :file, hint: image_tag(object.featured_image.url(:thumb))

    end

    inputs 'Gallery' do
      f.has_many :gallery_photos, heading: "" do |photos|
        photos.input :photo, as: :file, hint: image_tag(photos.object.photo.url(:thumb))
        if (photos.object.photo.present?)
          photos.input :_destroy, :as=> :boolean, :label => 'Remove Image'
        end
      end
    end

    inputs 'Project Videos' do
      f.has_many :project_videos, heading: "" do |video|
        video.input :video_url
      end
    end



    f.actions
  end

end
