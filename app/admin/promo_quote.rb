ActiveAdmin.register PromoQuote do
  menu label: "Devis de promotions"
  actions :all, except: [:edit]
  index download_links: [:xls]

  permit_params :promotion_id, :first_name, :last_name, :email,
    :phone, :code_postal, :ville, :loc, :pays

  index do
    selectable_column

    column :promotion
    column :first_name
    column :email
    column :phone
    column :code_postal
    column :ville
    column "Adresse", :loc
    column "Country", :pays
    actions
  end
  controller do
    def index
      @promo_quotes_list = PromoQuote.all
      index! do |format|
        format.xls {
          spreadsheet = PromoQuotesSpreadsheet.new @promo_quotes_list
          send_data spreadsheet.generate_xls, filename: 'tiba-promo-quotes.xls'
        }
      end
    end
  end
end
