ActiveAdmin.register SocialNetwork do
  menu label: "Réseaux sociaux"
  actions :all, except: [:new]

  permit_params :url, :activated

  form do |f|

    f.inputs "Name" do
      "<li><label>#{f.object.name}</label></li>".html_safe
    end

    f.object.name
    inputs do
      input :url
      input :activated
    end

    f.actions
  end
end
