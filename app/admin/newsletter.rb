ActiveAdmin.register Newsletter, as: 'Newsletter-Inscription' do
  actions :all, except: [:new]

  index download_links: []
  controller do
    def index
      index! do |format|
        format.xls {
          spreadsheet = NewslettersSpreadsheet.new @newsletters
          Rails.logger.info spreadsheet
          send_data spreadsheet.generate_xls, filename: 'tiba-newsletters.xls'
        }
      end
    end
  end
end
