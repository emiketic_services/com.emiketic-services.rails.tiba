ActiveAdmin.register Contact do
  actions :all, except: [:new]
  index download_links: [:xls]
  controller do
    def index
      index! do |format|
        @contact_list= Contact.all
        format.xls {
          spreadsheet = ContactsSpreadsheet.new @contact_list
          send_data spreadsheet.generate_xls, filename: 'tiba-contacts.xls'
        }
      end
    end
  end
end
