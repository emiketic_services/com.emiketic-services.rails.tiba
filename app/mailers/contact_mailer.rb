class ContactMailer < ApplicationMailer

  ActionMailer::Base.smtp_settings = {
    address:              "ssl0.ovh.net",
    port:                 25,
    user_name:            ENV['CONTACT_EMAIL'],
    password:             ENV['CONTACT_PASSWORD'],
    authentication:       'plain',
    enable_starttls_auto: true
  }

  default from: ENV['CONTACT_EMAIL']

  def forward_email(contact)
    @contact = contact
    mail(to: ENV['CONTACT_EMAIL_DEST'], subject: "Nouveau contact (#{@contact.email})")
  end
end
