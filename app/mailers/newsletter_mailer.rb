class NewsletterMailer < ApplicationMailer

  ActionMailer::Base.smtp_settings = {
    address:              "ssl0.ovh.net",
    port:                 25,
    user_name:            ENV['NEWSLETTER_EMAIL'],
    password:             ENV['NEWSLETTER_PASSWORD'],
    authentication:       'plain',
    enable_starttls_auto: true
  }

  default from: ENV['NEWSLETTER_EMAIL']
  def forward_newsletter(newsletter)
    @newsletter = newsletter
    mail(to: ENV['NEWSLETTER_EMAIL_DEST'], subject: "Demande d'inscription newsletter envoyer de la part #{@newsletter.email}")
  end
end
