class QuoteMailer < ApplicationMailer

  ActionMailer::Base.smtp_settings = {
    address:              "ssl0.ovh.net",
    port:                 25,
    user_name:            ENV['QUOTE_EMAIL'],
    password:             ENV['QUOTE_PASSWORD'],
    authentication:       'plain',
    enable_starttls_auto: true
  }

  default from: ENV['QUOTE_EMAIL']
  def forward_quote(quote)
    @quote = quote
    mail(to: ENV['QUOTE_EMAIL_DEST'], subject: "Demande de devis de la part #{@quote.email}")
  end
end
